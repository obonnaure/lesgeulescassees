Points = new Mongo.Collection("points");

if (Meteor.isClient) {
  // counter starts at 0
  Template.home.helpers({
    points: function () {
      return Points.find({}, {"limit": 10});
    }
  });

}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
